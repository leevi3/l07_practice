const express = require('express');
const router = express();
const port = 3000;

// Router endpoint
router.get("/",(req,res)=>res.send("welcome!"));

// Router endpoint to add two numbres together. Query parameters are "a" and "b"
router.get("/add",(req,res)=>{
    try {
        const sum = parseFloat(req.query.a) + parseFloat(req.query.b);
        res.send(sum.toString());
    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;